﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace textbook
{
    public partial class FirmNotepad : Form
    {
        bool b = false;//false表示是新建的
        bool s = true;//true表示已保存
        public FirmNotepad()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void rtxNotepad_TextChanged(object sender, EventArgs e)
        {
            //文本被修改后布尔类型s变为false，表示未保存
            s = false;
        }

        private void tsmiNew_Click(object sender, EventArgs e)
        {
            // 判断当前文件是否从磁盘打开，或者新建时文档不为空，并且文件未被保存
            if (b == true ||rtxNotepad.Text.Trim() != "")
            {
                if (s == false)//文件未保存  
                {
                    string result;
                    result = MessageBox.Show("文件尚未保存,是否保存?",
                "保存文件", MessageBoxButtons.YesNoCancel).ToString();//发出一个对话框，用户输入的东西返回成result
                    switch (result)
                    {
                        case "Yes":
                            if(b == true)
                            {
                                rtxNotepad.SaveFile(odlgNotepad.Filter);//保存的是那个的类型
                            }else if(sdlgNotepad.ShowDialog() == DialogResult.OK){
                                rtxNotepad.SaveFile(sdlgNotepad.FileName);
                            }
                            s = true;//文件不是新建的已保存
                            rtxNotepad.Text = "";
                            break;
                        case "No"://不保存，文本内容清空
                            b = false;
                            rtxNotepad.Text = "";
                            break;
                    }

                }
            }
        }

        private void stsNotepad_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void tsmiOpen_Click(object sender, EventArgs e)
        {
            if (b == true || rtxNotepad.Text.Trim() != "")
            {
                if(s == false)
                {
                    string result;
                    result = MessageBox.Show("文件尚未保存,是否保存?",
                "保存文件", MessageBoxButtons.YesNoCancel).ToString();
                    switch (result)
                    {
                        case "Yes":
                            if (b == true)
                            {
                                rtxNotepad.SaveFile(odlgNotepad.FileName);
                            }
                            else if (sdlgNotepad.ShowDialog() == DialogResult.OK)
                            {
                                rtxNotepad.SaveFile(sdlgNotepad.FileName);
                            }
                            s = true;
                            break;
                        case "No":
                            b = false;
                            rtxNotepad.Text = "";
                            break;
                    }
                }
            }
            odlgNotepad.RestoreDirectory = true;
            if(odlgNotepad.ShowDialog() == DialogResult.OK)
            {
                rtxNotepad.LoadFile(odlgNotepad.FileName);//打开
                b = true;
            }
            s = true;
        }

        private void tsmiSave_Click(object sender, EventArgs e)
        {
            if(b == true && rtxNotepad.Modified == true)//新建的文件的保存
            {
                rtxNotepad.SaveFile(odlgNotepad.FileName);
                s = true;
            }else if(b == false && rtxNotepad.Text.Trim()!="" && sdlgNotepad.ShowDialog() == DialogResult.OK)
            {
                rtxNotepad.SaveFile(sdlgNotepad.FileName);
                s = true;
                b = true;
                odlgNotepad.FileName = sdlgNotepad.FileName;
            }
        }

        private void tsmiSaveAs_Click(object sender, EventArgs e)
        {//另存为的代码
            if (sdlgNotepad.ShowDialog() == DialogResult.OK)
            {
                rtxNotepad.SaveFile(sdlgNotepad.FileName);
                s = true;
            }
        }
        //退出应用的代码
        private void tsmiClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //撤销操作
        private void tsmiUndo_Click(object sender, EventArgs e)
        {
            rtxNotepad.Undo();
        }
        //复制
        private void tsmiCopy_Click(object sender, EventArgs e)
        {
            rtxNotepad.Copy();
        }
        //剪切
        private void tsmiCut_Click(object sender, EventArgs e)
        {
            rtxNotepad.Cut();
        }
        //粘贴
        private void tsmiPaste_Click(object sender, EventArgs e)
        {
            rtxNotepad.Paste();
        }
        //全选
        private void tsmiSelectAll_Click(object sender, EventArgs e)
        {
            rtxNotepad.SelectAll();
        }
        //显示当前日期
        private void tssLbl2_Click(object sender, EventArgs e)
        {
            rtxNotepad.AppendText(System.DateTime.Now.ToString());
        }
        //自动换行
        private void tsmiAuto_Click(object sender, EventArgs e)
        {
            if(tsmiAuto.Checked == false)
            {
                tsmiAuto.Checked = true;
                rtxNotepad.WordWrap = true;
            }
            else
            {
                tsmiAuto.Checked = false;
                rtxNotepad.WordWrap = true;
            }
        }
        //修改字体
        private void tsmiFont_Click(object sender, EventArgs e)
        {
            if(fdlgNotepad.ShowDialog() == DialogResult.OK)
            {
                rtxNotepad.SelectionColor = fdlgNotepad.Color;
                rtxNotepad.SelectionFont = fdlgNotepad.Font;
            }
        }

        private void tsmiToolStrip_Click(object sender, EventArgs e)
        {
            Point point;
            if(tsmiToolStrip.Checked = true)
            {
                point = new Point(0, 24);                
                tsmiToolStrip.Checked = false;
                tlsNotepad.Visible = false;
                //设置多格式文本框左上角位置
                rtxNotepad.Location = point;
                rtxNotepad.Height += tlsNotepad.Height;
            }
            else
            {
                /* 显示工具栏时，多格式文本框左上角位置的位置为（0，49），
                    因为工具栏的高度为25，加上菜单的高度24后为49 */
                point = new Point(0, 49);
                tsmiToolStrip.Checked = true;
                tlsNotepad.Visible = true;
                tlsNotepad.Location = point;
                tlsNotepad.Height -= tlsNotepad.Height;
            }
        }
        //状态栏
        private void tsmiStatusStrip_Click(object sender, EventArgs e)
        {
            if(tsmiStatusStrip.Checked == true)
            {
                tsmiStatusStrip.Checked = false;
                stsNotepad.Visible = false;
                rtxNotepad.Height += stsNotepad.Height;
            }
            else
            {
                tsmiStatusStrip.Checked = true;
                stsNotepad.Visible = true;
                rtxNotepad.Height -= stsNotepad.Height;
            }
        }


    }
}
/*
 * 状态栏控件：stsNotepad
 * 打开对话框（OpenFileDialog）：odlgNotepad
 * 保存对话框（SaveFileDialog）：sdlgNotepad
 * 字体对话框（FontDialog）    ：fdlgNotepad
 * 计时器空间（Timer）         ：tmrNotepad
 * 多格式文本矿建RichTextBox   ：rtxtNotepad
 * 文本框（rtxtNotepad）、菜单（mnusNotepad）、工具栏（tlsNotepad）
 */




//MessageBox用于创建一个消息对话框